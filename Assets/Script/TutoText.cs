﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutoText : MonoBehaviour
{
	public Transform target;
	public float startpos = 0.0f;
	public float endpos = 2.0f;

	private float fadeOutTime = 1.0f;
	private Color originalColor;
	private Color clearColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	private Text text;
	private bool displayed = false;

	private void Start()
	{
		text = GetComponent<Text>();
		originalColor = text.color;
		text.color = clearColor;
	}

	private void Update()
	{
		if (!displayed && target.position.x >= startpos && target.position.x <= endpos && target.position.y > -10.0f)
		{
			displayed = true;
			StartCoroutine(FadeInRoutine());
		}
		else if (displayed && (target.position.x < startpos || target.position.x > endpos))
		{
			displayed = false;
			StartCoroutine(FadeOutRoutine());
		}
	}

	private IEnumerator FadeInRoutine()
	{
		for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
		{
			text.color = Color.Lerp(clearColor, originalColor, Mathf.Min(1, t / fadeOutTime));
			yield return null;
		}
	}

	private IEnumerator FadeOutRoutine()
	{
		for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
		{
			text.color = Color.Lerp(originalColor, clearColor, Mathf.Min(1, t / fadeOutTime));
			yield return null;
		}
	}
}
