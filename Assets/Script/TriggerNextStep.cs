﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNextStep : MonoBehaviour
{
	public GameController controller;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			controller.NextStep();
			gameObject.SetActive(false);
		}
	}
}
