﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFade : MonoBehaviour
{
	public float fadeOutTime = 2.0f;

	private Image img;
	private Color originalColor;

	private void Start()
	{
		img = gameObject.GetComponent<Image>();
		originalColor = img.color;
	}

	public void FadeIn()
	{
		StartCoroutine(FadeInRoutine());
	}

	public void FadeOut()
	{
		StartCoroutine(FadeOutRoutine());
	}

	private IEnumerator FadeInRoutine()
	{
		for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
		{
			img.color = Color.Lerp(Color.clear, originalColor, Mathf.Min(1, t / fadeOutTime));
			yield return null;
		}
	}

	private IEnumerator FadeOutRoutine()
	{
		for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
		{
			img.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t / fadeOutTime));
			yield return null;
		}
	}
}
