﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
	public CharacterController2D controller;
	public Animator animator;
	public float speed = 20.0f;
	public GameObject sideguardobj;
	public GameObject upguardobj;

	private Rigidbody2D rb2d;
	private float direction = 0.0f;
	private bool jump = false;
	private bool guard = false;
	private bool sideguard = true;

	private void Start()
	{
		rb2d = gameObject.GetComponent<Rigidbody2D>();
	}

	void Update()
    {
		// Move Input
		if (Input.GetAxisRaw("Horizontal") > 0)
			direction = speed;
		else if (Input.GetAxisRaw("Horizontal") < 0)
			direction = -speed;
		else
			direction = 0.0f;

		// Guard
		float guardangle = Mathf.Abs(Input.GetAxisRaw("Horizontal")) - Input.GetAxisRaw("Vertical");
		if (guardangle > 0.0f)
			sideguard = true;
		else if (guardangle < 0.0f)
			sideguard = false;

		if (Input.GetButtonDown("Guard"))
			guard = true;
		if (Input.GetButtonUp("Guard"))
			guard = false;

		if (guard)
		{
			direction *= 0.001f;
			if (sideguard)
			{
				animator.SetBool("SideGuard", true);
				sideguardobj.SetActive(true);
				animator.SetBool("UpGuard", false);
				upguardobj.SetActive(false);
			}
			else
			{
				animator.SetBool("SideGuard", false);
				sideguardobj.SetActive(false);
				animator.SetBool("UpGuard", true);
				upguardobj.SetActive(true);
			}
		}
		else
		{
			animator.SetBool("SideGuard", false);
			sideguardobj.SetActive(false);
			animator.SetBool("UpGuard", false);
			upguardobj.SetActive(false);
		}

		// Jump Input
		if (Input.GetButtonDown("Jump") && !guard)
		{
			jump = true;
			animator.SetBool("IsJumping", true);
		}

		animator.SetFloat("Speed", Mathf.Abs(direction));
	}

	private void FixedUpdate()
	{
		controller.Move(direction * Time.fixedDeltaTime, false, jump);
		jump = false;
	}

	public void OnLanding()
	{
		animator.SetBool("IsJumping", false);
	}

	public void MovePlayer(float move)
	{
		controller.Move(move * Time.fixedDeltaTime, false, false);
		animator.SetFloat("Speed", Mathf.Abs(move));
	}

	public void Stop()
	{
		direction = 0.0f;
		animator.SetFloat("Speed", direction);
		rb2d.velocity = new Vector2(0.0f, rb2d.velocity.y);
	}
}
