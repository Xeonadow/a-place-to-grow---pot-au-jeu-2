﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhantomKing : MonoBehaviour
{
	public GameController controller;
	public Transform target;
	public float speed;
	public float repelspeed = 20.0f;
	public float repeltime = 1.0f;

	private Rigidbody2D rb;
	private Vector3 lastpos;
	private bool m_FacingRight = false;

	void Start()
    {
		rb = gameObject.GetComponent<Rigidbody2D>();
		lastpos = transform.position;

	}

    void FixedUpdate()
    {
		Vector3 direction = target.position - transform.position;
		rb.MovePosition(transform.position + direction.normalized * speed * Time.deltaTime);
		if (Mathf.Sign(direction.x) > 0 && !m_FacingRight)
			Flip();
		else if (Mathf.Sign(direction.x) < 0 && m_FacingRight)
			Flip();
		//rb.velocity = direction.normalized * speed;
		lastpos = transform.position;
    }

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	public void Repel()
	{
		gameObject.GetComponent<Collider2D>().enabled = false;

		Vector3 dir = lastpos - transform.position;
		StartCoroutine(Die(dir.normalized));
	}

	private IEnumerator Die(Vector3 direction)
	{
		controller.NextStep();

		SpriteRenderer spriterenderer = gameObject.GetComponent<SpriteRenderer>();
		Color originalColor = spriterenderer.color;

		for (float t = 0.01f; t < repeltime; t += Time.deltaTime)
		{
			rb.MovePosition(transform.position + direction.normalized * speed * Time.deltaTime);
			spriterenderer.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t / repeltime));
			yield return null;
		}

		Destroy(gameObject);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.gameObject.tag == "Player")
			controller.GameOver();
		else if (collision.gameObject.tag == "Partner" && collision.gameObject.GetComponent<PartnerController>().getForm() < 2)
			controller.GameOver();
	}
}
