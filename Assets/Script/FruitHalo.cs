﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitHalo : MonoBehaviour
{
	public float halotime = 2.0f;

	private SpriteRenderer spriterenderer;
	private Color originalColor;
	private Color clearColor;
	private bool fadeOut = true;
	private bool fadeIn = false;
	private float realHaloTime;

	void Start()
    {
		spriterenderer = gameObject.GetComponent<SpriteRenderer>();
		originalColor = spriterenderer.color;
		clearColor = new Color(originalColor.r, originalColor.g, originalColor.b, 0.0f);
		realHaloTime = halotime / 2.0f;
    }

    // Update is called once per frame
    void Update()
    {
		if (fadeOut)
		{
			fadeOut = false;
			StartCoroutine(FadeOutRoutine());
		}

		if (fadeIn)
		{
			fadeIn = false;
			StartCoroutine(FadeInRoutine());
		}
	}

	private IEnumerator FadeInRoutine()
	{
		for (float t = 0.01f; t < realHaloTime; t += Time.deltaTime)
		{
			spriterenderer.color = Color.Lerp(clearColor, originalColor, Mathf.Min(1, t / realHaloTime));
			yield return null;
		}
		fadeOut = true;
	}

	private IEnumerator FadeOutRoutine()
	{
		for (float t = 0.01f; t < realHaloTime; t += Time.deltaTime)
		{
			spriterenderer.color = Color.Lerp(originalColor, clearColor, Mathf.Min(1, t / realHaloTime));
			yield return null;
		}
		fadeIn = true;
	}
}
