﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
	public Transform cam;
	public Transform bg1;
	public Transform bg2;
	public float speed = 0.5f;

	private float dist;
	private float leftwarppoint;
	private float rightwarppoint;
	//private Vector3 bg1initpos;
	//private Vector3 bg2initpos;
	//private int nexttp = 1;
	private float lastcamx;

	// Start is called before the first frame update
	void Start()
    {
		dist = Mathf.Abs(bg2.localPosition.x - bg1.localPosition.x);
		leftwarppoint = bg1.localPosition.x - dist;
		rightwarppoint = bg2.localPosition.x;
		//bg1initpos = bg1.position;
		//bg2initpos = bg2.position;
		lastcamx = cam.position.x;
	}

    // Update is called once per frame
    void LateUpdate()
    {
		Vector3 move = new Vector3(speed * (lastcamx - cam.position.x), 0.0f, 0.0f);

		MoveBgSprite(bg1, move);
		MoveBgSprite(bg2, move);

		/*bg1.position += move;
		bg2.position += move;

		if (nexttp == 1 && bg2.position.x <= initpos)
		{
			Vector3 pos = bg1.position;
			bg1.position = new Vector3(bg2.position.x + dist, pos.y, pos.z);
			nexttp = 2;
		}
		else if (nexttp == 2 && bg1.position.x <= initpos)
		{
			Vector3 pos = bg2.position;
			bg2.position = new Vector3(bg1.position.x + dist, pos.y, pos.z);
			nexttp = 1;
		}*/

		lastcamx = cam.position.x;
	}

	void MoveBgSprite(Transform bg, Vector3 move)
	{
		bg.localPosition += move;
		if (bg.localPosition.x <= leftwarppoint)
			bg.localPosition += new Vector3(dist * 2, 0.0f, 0.0f);
		else if (bg.localPosition.x > rightwarppoint)
			bg.localPosition -= new Vector3(dist * 2, 0.0f, 0.0f);
	}
}
