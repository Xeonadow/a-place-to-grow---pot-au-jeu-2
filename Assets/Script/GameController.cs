﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
	public GameObject player;
	public GameObject partner;

	public Image screenfade;
	public GameObject gameoverscreen;

	public AudioSource bgm;
	public List<AudioClip> musics;

	public GameObject background1;
	public GameObject background2;

	public Image title;
	public Text pressStart;

	public HealthBar healthbar;

	public Text protect;

	public List<GameObject> phantoms;

	public List<Flower> flowers;

	public GameObject endingScreen;

	private int healthpoint = 3;
	private bool gameover = false;
	private int step = 0;
	private PlayerController playercontroller;
	private PartnerController partnercontroller;
	private Rigidbody2D playerrb;
	private Rigidbody2D partnerrb;
	private ScreenFade screenfadescript;

	void Start()
	{
		playercontroller = player.GetComponent<PlayerController>();
		playercontroller.enabled = false;
		playerrb = player.GetComponent<Rigidbody2D>();

		healthbar.SetHealth(healthpoint);

		partnercontroller = partner.GetComponent<PartnerController>();
		partnerrb = partner.GetComponent<Rigidbody2D>();
		partnerrb.isKinematic = true;

		protect.enabled = false;

		screenfadescript = screenfade.GetComponent<ScreenFade>();
		screenfadescript.FadeOut();
	}

	void Update()
	{
		if (step == 0)
			StartScreen();
		else if (step == 2)
			GetPartner();
		else if (step == 4)
			PhantomWave1();
		else if (step == 6)
			PhantomWave2();
		else if (step == 8)
			ChangeZone();
		else if (step == 10)
			Berzerk();
		else if (step == 12)
			EndBerzerk();
		else if (step == 14)
			Ending();
	}

	public void NextStep()
	{
		step++;
	}

	public void TakeDamage()
	{
		healthpoint--;
		healthbar.SetHealth(healthpoint);
		if (healthpoint <= 0)
		{
			Destroy(partner);
			GameOver();
		}
	}

	public void GameOver()
	{
		if (!gameover)
		{
			gameover = true;
			playercontroller.Stop();
			playercontroller.enabled = false;
			StartCoroutine(Restart());
		}
	}

	private IEnumerator Restart()
	{
		yield return new WaitForSeconds(1.0f);

		screenfadescript.FadeIn();
		yield return new WaitForSeconds(2.0f);

		gameoverscreen.SetActive(true);

		screenfadescript.FadeOut();
		yield return new WaitForSeconds(2.0f);
		
		while (!Input.GetButtonDown("Submit"))
		{
			yield return null;
		}

		screenfadescript.FadeIn();
		yield return new WaitForSeconds(2.0f);
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	void StartScreen()
	{
		if (Input.GetButtonDown("Submit"))
		{
			StartCoroutine(Beginning());
			step++;
		}
	}

	private IEnumerator Beginning()
	{
		Color originalColor = title.color;
		Color clearColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
		float fadeOutTime = 2.0f;
		for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
		{
			title.color = Color.Lerp(originalColor, clearColor, Mathf.Min(1, t / fadeOutTime));
			pressStart.color = Color.Lerp(originalColor, clearColor, Mathf.Min(1, t / fadeOutTime));
			yield return null;
		}

		while (player.transform.position.x < -7.3f)
		{
			playercontroller.MovePlayer(playercontroller.speed);

			yield return null;
		}

		playercontroller.enabled = true;
	}

	void GetPartner()
	{
		playercontroller.Stop();
		playercontroller.enabled = false;
		StartCoroutine(Emerge());
		step++;
	}

	private IEnumerator Emerge()
	{
		yield return new WaitForSeconds(1.0f);

		while (partner.transform.position.y < -1.7)
		{
			partner.transform.position += new Vector3(0.0f, 0.5f * Time.deltaTime, 0.0f);
			yield return null;
		}

		protect.enabled = true;
		healthbar.FadeIn();
		yield return new WaitForSeconds(3.0f);

		playercontroller.enabled = true;
		partnerrb.isKinematic = false;
		partner.GetComponent<PartnerController>().enabled = true;
	}

	void PhantomWave1()
	{
		phantoms[0].SetActive(true);
		step++;
	}

	void PhantomWave2()
	{
		phantoms[1].SetActive(true);
		phantoms[2].SetActive(true);
		step++;
	}

	void ChangeZone()
	{
		StartCoroutine(ChangeZoneCoroutine());
		step++;
	}

	private IEnumerator ChangeZoneCoroutine()
	{
		screenfadescript.FadeIn();
		yield return new WaitForSeconds(2.0f);

		bgm.Stop();
		bgm.clip = musics[1];

		background1.SetActive(false);
		background2.SetActive(true);

		bgm.gameObject.transform.position = new Vector3(0.0f, -33.0f, -10.0f);
		playerrb.position = new Vector3(-8.6f, -36.6f, 0.0f);
		partnerrb.position = new Vector3(-11.3f, -37.7f, 0.0f);
		
		bgm.Play();

		screenfadescript.FadeOut();
		yield return new WaitForSeconds(2.0f);
		phantoms[3].SetActive(true);
	}

	void Berzerk()
	{
		StartCoroutine(BerzerkCoroutine());
		step++;
	}

	private IEnumerator BerzerkCoroutine()
	{
		partnercontroller.target = null;
		yield return new WaitForSeconds(2.0f);

		partnercontroller.MovePartner(20.0f);
		partner.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(4.5f);

		partner.GetComponent<AudioSource>().Stop();
		yield return new WaitForSeconds(2.0f);

		partnercontroller.target = player.transform;
	}

	void EndBerzerk()
	{
		bgm.Stop();
		bgm.clip = musics[2];
		bgm.Play();
		step++;
	}

	void Ending()
	{
		playercontroller.Stop();
		playercontroller.enabled = false;
		partnercontroller.obeyBehaviour = false;
		StartCoroutine(EndingRoutine());
		step++;
	}

	private IEnumerator EndingRoutine()
	{
		yield return new WaitForSeconds(1.0f);

		while (partner.transform.position.x < -238.6)
		{
			partnercontroller.MovePartner(15.0f);
			yield return null;
		}
		partnercontroller.Stop();
		//partner.GetComponent<Collider2D>().enabled = false;
		//partnerrb.gravityScale = 0.0f;

		yield return new WaitForSeconds(1.0f);

		while (partner.transform.position.y < -38.478)
		{
			partnerrb.MovePosition(partner.transform.position + new Vector3(0.0f, 0.5f * Time.deltaTime, 0.0f));
			yield return null;
		}

		yield return new WaitForSeconds(3.0f);

		foreach (Flower flow in flowers)
		{
			flow.Change();
			yield return new WaitForSeconds(0.8f);
		}

		yield return new WaitForSeconds(2.0f);

		partnercontroller.NextForm();
		yield return new WaitForSeconds(5.0f);

		screenfadescript.FadeIn();
		yield return new WaitForSeconds(2.0f);

		endingScreen.SetActive(true);

		screenfadescript.FadeOut();
	}
}
