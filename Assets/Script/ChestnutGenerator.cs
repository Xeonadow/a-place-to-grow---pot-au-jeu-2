﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestnutGenerator : MonoBehaviour
{
	public float delay = 2.0f;
	public GameObject chestnut;

	private float last;
	
    void Start()
    {
		last = Time.time;
    }

    void Update()
    {
		if (Time.time > last + delay)
		{
			Instantiate(chestnut, transform);
			last = Time.time;
		}
    }
}
