﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chestnut : MonoBehaviour
{
    void Update()
    {
		if (transform.position.y < -10.0f)
			Destroy(gameObject);
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		GameController controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

		if (collision.tag == "Partner")
			controller.TakeDamage();
	}
}
