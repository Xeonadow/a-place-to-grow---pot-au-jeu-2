﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phantom : MonoBehaviour
{
	public GameController gameController;
	public Transform target;
	public float speed = 10.0f;
	public float attacktime = 3.0f;
	public float attackrange = 5.0f;
	public float repelspeed = 20.0f;
	public float repeltime = 1.0f;

	private bool attacking = false;
	private Coroutine activecoroutine = null;
	private Vector3 lastpos;

	private bool m_FacingRight = false;

	void Start()
    {
		lastpos = transform.position;
    }

    void Update()
    {
		if (!attacking)
		{
			float dist = target.position.x - transform.position.x;

			if (Mathf.Sign(dist) > 0 && !m_FacingRight)
				Flip();
			else if (Mathf.Sign(dist) < 0 && m_FacingRight)
				Flip();
			
			if (Mathf.Abs(dist) > attackrange)
			{
				lastpos = transform.position;
				transform.position += new Vector3(Mathf.Sign(dist) * speed * Time.deltaTime, 0.0f, 0.0f);
			}
			else
				activecoroutine = StartCoroutine(Attack());
		}
    }

	private void Flip()
	{
		// Switch the way the player is labelled as facing.
		m_FacingRight = !m_FacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

	private IEnumerator Attack()
	{
		attacking = true;
		yield return new WaitForSeconds(1.0f);

		float dist = target.position.x - transform.position.x;
		Vector3 startpos = transform.position;
		Vector3 endpos = transform.position + new Vector3(dist * 2, 0.0f, 0.0f);
		float height = target.position.y - transform.position.y;


		for (float t = 0.01f; t < attacktime; t += Time.deltaTime)
		{
			lastpos = transform.position;
			transform.position = MathParabola.Parabola(startpos, endpos, height, Mathf.Min(1, t / attacktime));
			yield return null;
		}

		activecoroutine = null;
		attacking = false;
	}


	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Partner")
			gameController.TakeDamage();
	}

	public void Repel()
	{
		if (activecoroutine != null)
			StopCoroutine(activecoroutine);
		gameObject.GetComponent<Collider2D>().enabled = false;

		Vector3 dir = lastpos - transform.position;
		StartCoroutine(Die(dir.normalized));
	}

	private IEnumerator Die(Vector3 direction)
	{
		SpriteRenderer spriterenderer = gameObject.GetComponent<SpriteRenderer>();
		Color originalColor = spriterenderer.color;

		for (float t = 0.01f; t < repeltime; t += Time.deltaTime)
		{
			transform.position += direction * speed * Time.deltaTime;
			spriterenderer.color = Color.Lerp(originalColor, Color.clear, Mathf.Min(1, t / repeltime));
			yield return null;
		}

		Destroy(gameObject);
	}
}
