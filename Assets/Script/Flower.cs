﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flower : MonoBehaviour
{
	public GameObject sprite1;
	public GameObject sprite2;
	public float formstransition = 2.0f;

	private Color clearColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

	public void Change()
	{
		StartCoroutine(SwitchForms(sprite1, sprite2));
	}

	private IEnumerator SwitchForms(GameObject from, GameObject to)
	{
		SpriteRenderer spriterenderer1 = from.GetComponent<SpriteRenderer>();
		SpriteRenderer spriterenderer2 = to.GetComponent<SpriteRenderer>();
		Color ogcolor1 = spriterenderer1.color;
		Color ogcolor2 = spriterenderer2.color;

		spriterenderer2.color = clearColor;
		to.SetActive(true);

		for (float t = 0.01f; t < formstransition; t += Time.deltaTime)
		{
			spriterenderer1.color = Color.Lerp(ogcolor1, clearColor, Mathf.Min(1, t / formstransition));
			spriterenderer2.color = Color.Lerp(clearColor, ogcolor2, Mathf.Min(1, t / formstransition));
			yield return null;
		}

		from.SetActive(false);
	}
}
