﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : MonoBehaviour
{
	public Animator animator;
	public float jumpforce = 1400.0f;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			Rigidbody2D rb = collision.gameObject.GetComponent<Rigidbody2D>();
			//rb.velocity = Vector2.zero;
			//rb.AddForce(new Vector2(0.0f, jumpforce));
			rb.velocity = Vector2.zero;
			animator.SetTrigger("Jump");
			StartCoroutine(Jump(rb));
		}
	}

	private IEnumerator Jump(Rigidbody2D rb)
	{
		yield return new WaitForSeconds(0.08f);
		rb.AddForce(new Vector2(0.0f, jumpforce));
	}
}
