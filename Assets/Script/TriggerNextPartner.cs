﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerNextPartner : MonoBehaviour
{
	public GameController controller;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Partner")
		{
			controller.NextStep();
			gameObject.SetActive(false);
		}
	}
}
