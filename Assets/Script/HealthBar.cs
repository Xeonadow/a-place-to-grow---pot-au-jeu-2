﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
	public float fadeTime = 2.0f;
	public Sprite fullHeart;
	public Sprite emptyHeart;
	public List<Image> hearts;

	private Color originalColor;
	private Color clearColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

	private void Start()
	{
		originalColor = hearts[1].color;
		foreach (Image image in hearts)
		{
			image.color = clearColor;
		}
	}

	public void SetHealth(int health)
	{
		int i = 1;
		foreach (Image image in hearts)
		{
			if (i <= health)
				image.sprite = fullHeart;
			else
				image.sprite = emptyHeart;
			i++;
		}
	}

	public void FadeIn()
	{
		StartCoroutine(FadeInRoutine());
	}

	private IEnumerator FadeInRoutine()
	{
		for (float t = 0.01f; t < fadeTime; t += Time.deltaTime)
		{
			foreach (Image image in hearts)
			{
				image.color = Color.Lerp(clearColor, originalColor, Mathf.Min(1, t / fadeTime));
			}
			yield return null;
		}
	}
}
