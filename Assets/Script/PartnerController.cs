﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartnerController : MonoBehaviour
{
	public PlayerController playercontroller;
	public CharacterController2D controller;
	public Carnivorous carnivorous;
	public float distance = 3.0f;
	public float heightdist = 3.0f;
	public float formstransition = 2.0f;
	public List<GameObject> formslist;
	public Transform forms;
	public Transform target;
	public Animator animator;
	public bool obeyBehaviour = true;

	private int form = 0;
	private Color clearColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	private Rigidbody2D rb2d;
	private float direction = 0.0f;
	private bool jump = false;
	private bool isjumping = false;

	private void Start()
	{
		rb2d = gameObject.GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		animator.SetFloat("Speed", Mathf.Abs(direction));
	}

	void FixedUpdate()
    {
		if (obeyBehaviour)
		{
			if (form == 0)
				SproutBehaviour();
			else if (form == 1)
				MushroomBehaviour();
			else if (form == 2)
				CarnivorousBehaviour();
			else if (form == 3)
				BudBehaviour();
		}
	}

	void SproutBehaviour()
	{
		direction = 0.4f * playercontroller.speed;
		controller.Move(direction * Time.fixedDeltaTime, false, false);
	}

	void MushroomBehaviour()
	{
		Vector3 distdiff = playercontroller.transform.position - transform.position;
		if (distdiff.magnitude > distance && Mathf.Abs(distdiff.x) > 2.0f)
			direction = distdiff.x > 0 ? playercontroller.speed : -playercontroller.speed;
		else
			direction = 0.0f;

		if (rb2d.velocity.y <= 0.0f)
			isjumping = false;

		//if (((playercontroller.transform.position.y - transform.position.y > heightdist && Mathf.Abs(distdiff.x) > 2.0f) || Mathf.Abs(distdiff.x) > 6.0f) && !isjumping)
		if (((distdiff.y > heightdist && Mathf.Abs(distdiff.x) > 2.0f) || Mathf.Abs(distdiff.x) > 6.0f) && !isjumping)
		{
			jump = true;
			isjumping = true;
		}

		controller.Move(direction * Time.fixedDeltaTime, false, jump);
		jump = false;
	}
	
	void CarnivorousBehaviour()
	{
		if (target != null)
		{
			carnivorous.Attack();
			Vector3 distdiff = target.position - transform.position;
			direction = distdiff.x > 0 ? 0.9f * playercontroller.speed : -0.9f * playercontroller.speed;

			if (rb2d.velocity.y <= 0.0f)
				isjumping = false;

			if ((distdiff.y > heightdist || Mathf.Abs(distdiff.x) > 16.0f) && !isjumping)
			{
				jump = true;
				isjumping = true;
			}

			controller.Move(direction * Time.fixedDeltaTime, false, jump);
			jump = false;
		}
		else
			carnivorous.StopAttack();
	}

	void BudBehaviour()
	{
		Vector3 distdiff = playercontroller.transform.position - transform.position;
		if (distdiff.magnitude > distance && Mathf.Abs(distdiff.x) > 2.0f)
			direction = distdiff.x > 0 ? playercontroller.speed : -playercontroller.speed;
		else
			direction = 0.0f;

		controller.Move(direction * Time.fixedDeltaTime, false, false);
		jump = false;
	}

	public void NextForm()
	{
		if (form == 1)
		{
			heightdist = 5.0f;
		}
		StartCoroutine(SwitchForms(formslist[form], formslist[form + 1]));
	}

	private IEnumerator SwitchForms(GameObject from, GameObject to)
	{
		int curform = form;
		form = -1;
		SpriteRenderer spriterenderer1 = from.GetComponent<SpriteRenderer>();
		SpriteRenderer spriterenderer2 = to.GetComponent<SpriteRenderer>();
		Color ogcolor1 = spriterenderer1.color;
		Color ogcolor2 = spriterenderer2.color;

		spriterenderer2.color = clearColor;
		to.SetActive(true);

		for (float t = 0.01f; t < formstransition; t += Time.deltaTime)
		{
			spriterenderer1.color = Color.Lerp(ogcolor1, clearColor, Mathf.Min(1, t / formstransition));
			spriterenderer2.color = Color.Lerp(clearColor, ogcolor2, Mathf.Min(1, t / formstransition));
			yield return null;
		}

		from.SetActive(false);
		form = curform + 1;
	}

	public int getForm()
	{
		return form;
	}

	public void MovePartner(float move)
	{
		controller.Move(move * Time.fixedDeltaTime, false, false);
		animator.SetFloat("Speed", Mathf.Abs(move));
	}

	public void Stop()
	{
		direction = 0.0f;
		animator.SetFloat("Speed", direction);
		rb2d.velocity = new Vector2(0.0f, rb2d.velocity.y);
	}

	void Frame1()
	{
		forms.localPosition = new Vector3(0.0f, 0.168f, 0.0f);
	}

	void Frame2()
	{
		forms.localPosition = new Vector3(0.0f, 0.188f, 0.0f);
	}

	void Frame3()
	{
		forms.localPosition = new Vector3(0.0f, 0.21f, 0.0f);
	}

	void Frame4()
	{
		forms.localPosition = new Vector3(0.0f, 0.12f, 0.0f);
	}

	void Frame5()
	{
		forms.localPosition = new Vector3(0.0f, 0.208f, 0.0f);
	}
}
