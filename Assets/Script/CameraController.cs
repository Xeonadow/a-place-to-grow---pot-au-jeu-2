﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
	public Transform player;
	public float wiggleroom = 1.0f;

    void LateUpdate()
    {
		float rightdiff = player.position.x - (transform.position.x + wiggleroom);
		float leftdiff = player.position.x - (transform.position.x - wiggleroom);

		if (rightdiff > 0)
			transform.position += new Vector3(rightdiff, 0.0f, 0.0f);
		else if (leftdiff < 0)
			transform.position += new Vector3(leftdiff, 0.0f, 0.0f);

		if (transform.position.x < 0)
			transform.position += new Vector3(-transform.position.x, 0.0f, 0.0f);
	}
}
