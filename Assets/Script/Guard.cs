﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guard : MonoBehaviour
{
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Phantom")
			collision.gameObject.GetComponent<Phantom>().Repel();
		else if (collision.tag == "Chestnut")
			Destroy(collision.gameObject);
	}
}
