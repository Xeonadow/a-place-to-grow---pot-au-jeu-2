﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour
{
	public SpriteRenderer spriterenderer;
	public float fadeOutTime = 2.0f;

	private Color originalColor;
	private Color clearColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	private bool used = false;

	private void Start()
	{
		originalColor = spriterenderer.color;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Partner" && !used)
		{
			collision.gameObject.GetComponent<PartnerController>().NextForm();
			StartCoroutine(FadeOutRoutine());
		}
	}

	private IEnumerator FadeOutRoutine()
	{
		for (float t = 0.01f; t < fadeOutTime; t += Time.deltaTime)
		{
			spriterenderer.color = Color.Lerp(originalColor, clearColor, Mathf.Min(1, t / fadeOutTime));
			yield return null;
		}
		Destroy(gameObject);
	}
}
