﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carnivorous : MonoBehaviour
{
	public GameController game;
	public PartnerController partnercontroller;
	public Animator animator;
	public Collider2D hitbox;
	
    // Update is called once per frame
    public void Attack()
    {
		animator.SetBool("Attack", true);
    }

	public void StopAttack()
	{
		animator.SetBool("Attack", false);
	}

	public void EnableHitbox()
	{
		hitbox.enabled = true;
	}

	public void DisableHitbox()
	{
		hitbox.enabled = false;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player" && partnercontroller.target.gameObject.name == "Player")
			game.GameOver();
		else if (collision.tag == "PhantomKing")
			collision.gameObject.GetComponent<PhantomKing>().Repel();
	}
}
