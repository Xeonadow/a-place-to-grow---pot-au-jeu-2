﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
	public float strength = 0.2f;
	public float speed = 0.2f;

	private Vector3 initpos;

    void Start()
    {
		initpos = transform.localPosition;
    }

    void Update()
    {
		transform.localPosition = initpos + new Vector3(0.0f, strength * Mathf.Sin(Time.time * speed), 0.0f);
    }
}
